$(document).ready(function () {

    var controller = new NotesController();
    var ajax = new notesAjax();
    var currrentlySelectedNoteId = undefined;

    ajax.getTasks(controller.addNote);

    /*Events listeners*/
    $("#add-main-btn").on("click", function () {
        controller.hideShowSection("hidden-add-note-section", "flex");
    });
    $("#edit-main-btn").on("click", function () {
        controller.hideShowSection("hidden-edit-note-section", "flex");
    });
    $("#add-cancel-btn").on("click", function () {
        controller.hideShowSection("hidden-add-note-section", "none");
        controller.disableButtons("#delete-btn");
        controller.disableButtons("#edit-main-btn");
    });
    $("#edit-cancel-btn").on("click", function () {
        controller.hideShowSection("hidden-edit-note-section", "none");
        controller.disableButtons("#delete-btn");
        controller.disableButtons("#edit-main-btn");
    });
    $("#add-note-btn").on("click", function () {
        var data = $("#add-note-text")[0].value;
        var callback = controller.addNote;
        ajax.postTask(data, callback);
    });
    $("#edit-send-btn").on("click", function () {
        var data = $("#edit-note-text")[0].value;
        var callback = controller.editTask;
        ajax.editTask(currrentlySelectedNoteId, data, callback);
    });
    $("body").on("focusin", ".notes", function () {
        controller.enableButtons("#delete-btn");
        controller.enableButtons("#edit-main-btn");
        currrentlySelectedNoteId = $(".notes:focus").attr('id');
    });
    $("#delete-btn").on("click", function () {
        var data = controller.lastFocusedElement.id;
        var callback = controller.deleteNote;
        ajax.deleteTask(data, callback);
        controller.disableButtons("#delete-btn");
        controller.disableButtons("#edit-main-btn");
    });
});
