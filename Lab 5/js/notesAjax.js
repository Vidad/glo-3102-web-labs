var notesAjax = function () {

    this.getTasks = function (callback) {
        $.ajax({
            url: "http://127.0.0.1:5000/tasks",
            method: "GET",
            dataType: "json"
        }).done(function (response) {
            $(response.tasks).each(function (index) {
                callback($(this)[0]);
            });
        });
    };

    this.postTask = function (data, callback) {
        $.ajax({
            url: "http://127.0.0.1:5000/tasks",
            method: "POST",
            data: JSON.stringify({task: data}),
            dataType: "json",
            contentType: "application/json"
        }).done(function (response) {
            callback(response.task);
        });
    };

    this.editTask = function (id, data, callback) {
        $.ajax({
            url: "http://127.0.0.1:5000/tasks/" + id,
            method: "PUT",
            data: JSON.stringify({task: data}),
            dataType: "json",
            contentType: "application/json"
        }).done(function (response) {
            callback(response.task);
        });
    };

    this.deleteTask = function (data, callback) {
        $.ajax({
            url: "http://127.0.0.1:5000/tasks/" + data,
            method: "DELETE",
            dataType: "json",
        }).done(function (response) {
            callback(response, data);
        });
    };
};
