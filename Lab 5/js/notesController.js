function NotesController() {
    var self = this;
    this.lastFocusedElement = undefined;

    this.hideShowSection = function (sectionID, displayStyle) {
        var hiddenSection = document.getElementById(sectionID);
        hiddenSection.style.display = displayStyle;
    };

    this.addNote = function (data) {
        $("#notes-list").append("<li id=" + data.id + "><a id=" + data.id + " href='#' class='notes'><p id=" + data.id + ">" + data.task + "</p></a></li>");
        self.hideShowSection("hidden-add-note-section", "none");
    };

    this.enableButtons = function (buttonID) {
        $(buttonID).prop("disabled", false);
        self.lastFocusedElement = $(":focus")[0];
    };

    this.disableButtons = function (buttonID) {
        $(buttonID).prop("disabled", true);
    };

    this.deleteNote = function (data, noteId) {
        if (data.result) {
            $("#" + noteId).remove();
        }
    };

    this.editTask = function (task) {
        self.hideShowSection("hidden-edit-note-section", "none");
        self.disableButtons("#delete-btn");
        self.disableButtons("#edit-main-btn");
        $("#" + task.id).remove();
        self.addNote(task);
    };
};
