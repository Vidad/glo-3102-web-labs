$(document).ready(function () {

    var app = app || {};

    var location = new Location();
    location.fetch().done(function (data) {
        var navbarView = new NavbarView({model: data.location});
        navbarView.render();

        var weekWeather = new Week();
        weekWeather.url = "//api.wunderground.com/api/4dd6e18b4fcae40b/forecast7day/q/" + data.location.lat + "," + data.location.lon + ".json";
        weekWeather.fetch({datatype: "jsonp"}).done(function () {
            var contentView = new WeatherView({collection: weekWeather});
            contentView.render();
        });
    });
});