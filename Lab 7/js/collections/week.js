Week = Backbone.Collection.extend({
    parse: function (data) {
        var weekForecast = data.forecast.simpleforecast.forecastday;
        return weekForecast;
    },
    model: Day
});