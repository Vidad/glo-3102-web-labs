var app = app || {};

NavbarView = Backbone.View.extend({
    el: '#navbar',
    render: function () {
        var template = _.template($("#navbar-template").html());
        this.$el.html(template({location: this.model}));
        this.$el.show();
    }
});



