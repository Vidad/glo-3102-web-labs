var app = app || {};

WeatherView = Backbone.View.extend({
    el: '#content',
    render: function () {
        var template = _.template($("#weather-template").html());
        this.$el.html(template({days: this.collection.toJSON()}));
        this.$el.show();
    }
});




