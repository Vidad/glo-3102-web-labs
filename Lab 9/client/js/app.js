var app = app || {};

$(document).ready(function () {
        var loginView = new LoginView();
        var userProfileView = new UserProfileView();

        var getProfileData = function (token) {
            $.ajax({
                url: "http://localhost:5000/userprofile",
                type: 'GET',
                contentType: "application/json",
                headers: {"Authorization": token}
            }).done(function (data) {
                showProfile(data);
            }).fail(function () {
                showLogin();
            });
        };

        function authenticate(showLogin) {
            var cookie = Cookies.get('lab9Cookie');
            if (cookie === undefined) {
                showLogin();
            } else {
                getProfileData(cookie)
            }
        }

        var showProfile = function (user) {
            userProfileView.render({user: user});
        };

        var showLogin = function () {
            loginView.render(getProfileData);
        };

        authenticate(showLogin);
    }
);