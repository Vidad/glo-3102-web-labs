var app = app || {};

LoginView = Backbone.View.extend({
    el: '#content'
    , render: function (authenticatedCallback) {
        var template = _.template($("#login-page-template").html());
        this.$el.html(template());
        this.$el.show();
        this.authenticatedCallback = authenticatedCallback;
    }
    , events: {
        'click #login-btn': 'login'
    }
    , login: function () {
        var self = this;
        var username = $('#username').val();
        var password = $('#password').val();
        var passwordEncoded = window.btoa(password);

        $.ajax({
            url: "http://localhost:5000/authorize",
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({ username: username, password: passwordEncoded})
        }).done(function (data) {
            Cookies.set('lab9Cookie', data.token);
            console.log("Successfully authenticated, getting user profile");
            self.authenticatedCallback(data.token);
        }).fail(function () {
            alert("Wrong combination of username/password")
            console.log("Failed to authenticate");
        });
    }
});



