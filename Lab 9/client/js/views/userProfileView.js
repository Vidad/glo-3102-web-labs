var app = app || {};

UserProfileView = Backbone.View.extend({
    el: '#content'
    , render: function (user) {
        var template = _.template($("#user-profile-template").html());
        this.$el.html(template(user));
        this.$el.show();
    }
});



