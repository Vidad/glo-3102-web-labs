function findById(id, list) {
    var index = -1;
    for (var i = 0; i < list.length; i++) {
        if (list[i].id == id) {
            index = i;
            break;
        }
    }
    return index;
}

exports.findById = findById;