var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var search = require('./search.js');

var app = express();
app.use(cors());
app.use(bodyParser.json());

var tasks = [];

app.get('/tasks', function (request, response) {
    response.send(tasks);
});

app.get('/tasks/:id', function (request, response) {
    var indexToGet = search.findById(request.params.id, tasks);

    if (indexToGet === -1) {
        response.status(404).send("Task id could not be found");
    }
    else {
        response.send(tasks[indexToGet]);
    }
});

app.post('/tasks', function (request, response) {
    if (request.body.task === undefined) {
        response.status(400).send("Task is empty");
    } else {
        var newTask = {
            'id': Math.floor((Math.random() * 12000000000) + 1),
            'task': request.body.task
        };
        tasks.push(newTask);
        response.status(201).send(newTask);
    }
});

app.put('/tasks/:id', function (request, response) {
    var indexToModify = search.findById(request.params.id, tasks);

    if (indexToModify === -1) {
        response.status(404).send("Task id could not be found");
    }
    else {
        tasks[indexToModify].task = request.body.task;
        response.send(tasks[indexToModify]);
    }
});

app.delete('/tasks/:id', function (request, response) {
    var indexToDelete = search.findById(request.params.id, tasks);

    if (indexToDelete === -1) {
        response.status(404).send("Task id could not be found");
    }
    else {
        tasks.splice(indexToDelete, 1);
        response.sendStatus(200);
    }
});

app.listen(5000);
console.log('Listening on port 5000...');
