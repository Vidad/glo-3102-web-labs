addEventListenerCustom(document.getElementsByClassName('imageRadio'), 'click', selectImageByIndex);
addEventListenerCustom(document.getElementsByClassName('arrow'), 'click', changeImageWithArrows);

var timer = setInterval(function() {changeToNextImage('right');}, 3000);

function addEventListenerCustom (elements, eventName, callbackFunction) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener(eventName, callbackFunction);
    }
}

function hideAllOtherImages (images, index) {
    for (var i = 0; i < images.length; i++){
        if (i != index) {
            images[i].style.display = 'none';
        }
    }
}

function showImage(images, index) {
    images[index].style.display = 'block';
}

function findNextIndex(arrowType, currentIndex) {
    if (arrowType == 'right'){
        switch (currentIndex) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 0;
        }
    }
    else {
        switch (currentIndex) {
            case 0:
                return 2;
            case 1:
                return 0;
            case 2:
                return 1;
        }
    }

}

function findCurrentIndex (elements) {
    for (var i = 0; i < elements.length; i++){
        if (elements[i].checked) {
            return parseInt(elements[i].value);
        }
    }
}

function selectImageByIndex(event) {
    var index = parseInt(event.currentTarget.value);
    var carouselImages = window.document.getElementById("carousel-images");
    hideAllOtherImages(carouselImages.children, index);
    showImage(carouselImages.children, index);
    resetInterval();

}

function triggerClickOnRadioButton(button, radioButtons, nextIndex) {
    var eventToTrigger = document.createEvent('Event');
    eventToTrigger.initEvent('click', true, true);
    button.dispatchEvent(eventToTrigger);
    radioButtons[nextIndex].checked = true;

}

function changeToNextImage (arrowType) {
    var radioButtons = document.getElementsByClassName('imageRadio');
    var currentIndex = findCurrentIndex(radioButtons);
    var nextIndex = findNextIndex(arrowType, currentIndex);
    var button = radioButtons[nextIndex];
    triggerClickOnRadioButton(button, radioButtons, nextIndex);

}

function changeImageWithArrows(event) {
    var arrowType = event.currentTarget.dataset.type;
    changeToNextImage(arrowType);
    resetInterval();

}

function resetInterval() {
    clearInterval(timer);
    timer = setInterval(function() {changeToNextImage('right');}, 3000);
}