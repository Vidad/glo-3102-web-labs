$(document).ready(function () {

    /*Calculator instance and display global variable*/
    var calculator = new Calculator();
    var display = $("#result");
    var secondOperandEnteredCallback = undefined;

    /*Geolocalisation functions*/
    var displayGeolocation = function () {
        if (navigator.geolocation) {
            requestLocation();
        }
        else {
            alert("Geolocation is not supported by your browser");
        }
    };

    var requestLocation = function () {
        var timeoutVal = 10 * 1000 * 1000;
        navigator.geolocation.getCurrentPosition(
            displayPosition,
            displayGeolocationError,
            {
                enableHighAccuracy: true, timeout: timeoutVal,
                maximumAge: 0
            }
        );
    };

    var displayPosition = function (position) {
        $("#latitude").text(position.coords.latitude);
        $("#longitude").text(position.coords.longitude);
    };

    var displayGeolocationError = function (error) {
        $("#latitude").text("Error");
        $("#longitude").text("Error");
    };

    /*Calculator functions*/
    var clearEquation = function () {
        calculator.clear();
        displayResult();
    };

    var showNumber = function (event) {
        if (display.text() == "0") {
            display.text("");
        }
        display.text(display.text() + event.currentTarget.value);
    }

    var setMemory = function () {
        calculator.setMemory(display.text());
    };

    var getMemory = function () {
        display.text(calculator.getMemory());
    };

    var displayResult = function () {
        var equationResult = calculator.equals();
        display.text(!equationResult ? 0 : equationResult);
    };

    /*Events listeners*/
    $("#clear").on("click", clearEquation);
    $("#position").on("click", displayGeolocation);
    $("#left-button-grid > .row > input:not(#clear)").on("click", showNumber);
    $("#setmemory").on("click", setMemory);
    $("#getmemory").on("click", getMemory);

    $("#sin").on("click", function () {
        var valueInCalculator = parseFloat(display.text());
        calculator.sin(valueInCalculator);
        display.text(calculator.equals);
    });

    $("#cos").on("click", function () {
        var valueInCalculator = parseFloat(display.text());
        calculator.cos(valueInCalculator);
        display.text(calculator.equals);
    });

    $("#tan").on("click", function () {
        var valueInCalculator = parseFloat(display.text());
        calculator.tan(valueInCalculator);
        display.text(calculator.equals);
    });

    $("#division").on("click", function () {
        if (secondOperandEnteredCallback !== undefined && display.text() === "") {
            if(display.text() === ""){
                console.log("display text:" + display.text());
            }

            var inputValue = parseFloat(display.text());
            secondOperandEnteredCallback(inputValue);
        }
        calculator.value(parseFloat(display.text()));
        display.text("");
        secondOperandEnteredCallback = function (value) {
            calculator.divide(value);
            secondOperandEnteredCallback = undefined;
        };
    });

    $("#multiplication").on("click", function () {
        if (secondOperandEnteredCallback !== undefined) {
            var inputValue = parseFloat(display.text());
            secondOperandEnteredCallback(inputValue);
        }
        calculator.value(parseFloat(display.text()));
        display.text("");
        secondOperandEnteredCallback = function (value) {
            calculator.multiply(value);
            secondOperandEnteredCallback = undefined;
        };
    });

    $("#addition").on("click", function () {
        if (secondOperandEnteredCallback !== undefined) {
            var inputValue = parseFloat(display.text());
            secondOperandEnteredCallback(inputValue);
        }
        calculator.value(parseFloat(display.text()));
        display.text("");
        secondOperandEnteredCallback = function (value) {
            calculator.add(value);
            secondOperandEnteredCallback = undefined;
        };
    });

    $("#substraction").on("click", function () {
        if (secondOperandEnteredCallback !== undefined) {
            var inputValue = parseFloat(display.text());
            secondOperandEnteredCallback(inputValue);
        }
        calculator.value(parseFloat(display.text()));
        display.text("");
        secondOperandEnteredCallback = function (value) {
            calculator.subtract(value);
            secondOperandEnteredCallback = undefined;
        };
    });

    $("#equals").on("click", function () {
        if (secondOperandEnteredCallback !== undefined) {
            var inputValue = parseFloat(display.text());
            secondOperandEnteredCallback(inputValue);
        }
        displayResult();
    });

    $("#factorial").on("click", function () {
        var inputValue = parseFloat(display.text());
        calculator.factorial(inputValue);
        display.text(calculator.equals);
    });
});
