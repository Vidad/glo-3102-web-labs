var myCalculator = new Calculator()
console.log("initial calculator value:");
myCalculator.equals();

console.log("Addition: 3 + 3=");
myCalculator.plus(3).plus(3).equals();

console.log("Substraction: 15 - 5=");
myCalculator.reset();
myCalculator.plus(15).minus(5).equals();

console.log("Multiplication: 4 * 4=");
myCalculator.reset();
myCalculator.plus(4).times(4).equals();

console.log("Division: 15 / 5=");
myCalculator.reset();
myCalculator.plus(15).divideBy(5).equals();

console.log("Sinus: sin(0)=");
myCalculator.reset();
myCalculator.sin().equals();

console.log("Cosinus: cos(0)=");
myCalculator.reset();
myCalculator.cos().equals();

console.log("Tangent: tan(0)=");
myCalculator.reset();
myCalculator.tan().equals();

console.log("Memorize 5:")
myCalculator.reset();
console.log("Current value: ");
myCalculator.plus(5).equals()
myCalculator.memorize();
console.log("New value: ");
myCalculator.plus(5).equals()
console.log("Memorized value: " + myCalculator.getMemorizedValue());

console.log("Factorial 5:")
myCalculator.reset();
myCalculator.plus(5).factorial().equals();
