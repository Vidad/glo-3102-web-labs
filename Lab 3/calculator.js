function Calculator() {
    this.value = 0;
    this.memorizedValue = undefined;

    this.plus = function(number) {
        this.value += number;
        return this;
    };

    this.minus = function(number) {
        this.value -= number;
        return this;
    };

    this.divideBy = function(number) {
        this.value /= number;
        return this;
    };

    this.times = function(number) {
        this.value *= number;
        return this;
    };

    this.sin = function() {
        this.value = Math.sin(this.value);
        return this;
    };

    this.cos = function() {
        this.value = Math.cos(this.value);
        return this;
    };

    this.tan = function() {
        this.value = Math.tan(this.value);
        return this;
    };

    this.equals = function() {
        console.log(this.value);
    };

    this.reset = function() {
        this.value = 0;
    };

    this.memorize = function(){
        this.memorizedValue = this.value;
        return this;
    };

    this.getMemorizedValue = function(){
        return this.memorizedValue;
    };

    this.factorial = function() {
        this.value = calculateFactorial(this.value);
        return this;
    };

    var calculateFactorial = function(number) {
        if (number === 0) {
            return 1;
        }
        return number * calculateFactorial(number - 1);
    };

};
