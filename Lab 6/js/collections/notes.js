NotesCollection = Backbone.Collection.extend({
    url: "http://localhost:5000/tasks"
    , parse: function (data) {
        return data.tasks;
    }
    , model: Note
});