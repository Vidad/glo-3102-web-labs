var app = app || {};

AddNoteView = Backbone.View.extend({
    el: '#hidden-add-note-section'
    , render: function () {
        var template = _.template($("#add-note-template").html());
        this.$el.html(template());
        this.$el.show();
    }
    , events: {
        'click #add-note-btn': 'createNote',
        'click #add-cancel-btn': 'hideView'
    }
    , createNote: function () {
        var self = this;
        var data = $("#add-note-text")[0].value;
        self.collection.create({task: data});
        this.hideView();
    }
    , hideView: function () {
        this.$el.hide();
        $("#delete-btn").prop('disabled', true);
        $("#edit-main-btn").prop('disabled', true);
    }
});



