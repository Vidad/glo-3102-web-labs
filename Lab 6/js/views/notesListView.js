var app = app || {};

AllNotesView = Backbone.View.extend({
    el: '#notes-section'
    , initialize: function () {
        _.bindAll(this, 'render');
        var self = this;
        self.collection.bind('sync', function () {
           self.render();
        });
        self.model.bind('sync remove', function () {
            self.collection.fetch();
        });
    }
    , render: function () {
        var self = this;
        var template = _.template($("#notes-section-template").html());
        self.$el.html(template({notes: self.collection.toJSON()}));

    }
});



