var app = app || {};

EditNoteView = Backbone.View.extend({
    el: '#hidden-edit-note-section'
    , render: function () {
        var text = $(".mySelected").text();
        var note = new Object();
        note.text = text;
        var template = _.template($("#edit-note-template").html());
        this.$el.html(template({note}));
        this.$el.show();
    }
    , events: {
        'click #edit-note-btn': 'editNote',
        'click #edit-cancel-btn': 'hideView'
    }
    , editNote: function () {
        var self = this;
        var data = $("#edit-note-text")[0].value;
        var id = $(".mySelected").attr('id');
        self.model.set('id', id);
        self.model.set('task', data);
        self.model.save();
        this.hideView();
        this.disableButtons();
    }
    , deleteNote: function () {
        var self = this;
        var id = $(".mySelected").attr('id');
        self.model.set('id', id);
        self.model.destroy();
        this.disableButtons();
    }
    , hideView: function () {
        this.$el.hide();

    },
    disableButtons: function () {
        $("#delete-btn").prop('disabled', true);
        $("#edit-main-btn").prop('disabled', true);
    }
});




