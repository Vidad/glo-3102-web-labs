$(document).ready(function () {

    var app = app || {};

    app.notes = new NotesCollection();
    app.note = new Note();

    app.notesView = new AllNotesView({
        collection: app.notes,
        model: app.note
    });
    app.notes.fetch();

    app.addNoteView = new AddNoteView({
        collection: app.notes
    });

    app.editNoteView = new EditNoteView({
        model: app.note
    });

    /*Events listeners*/
    $("#add-main-btn").on("click", function () {
        app.addNoteView.render();
    });
    $("#edit-main-btn").on("click", function () {
        app.editNoteView.render();
    });
    $("#delete-btn").on("click", function () {
        app.editNoteView.deleteNote();
    });

    /*To be able to select a note (one at a time)*/
    $("body").on("click", ".notes", function () {
        $.each($(".mySelected"), function (i, val) {
            $(val).Unselect();
        });
        $(this).hasClass('mySelected')?$(this).Unselect() : $(this).Select();
        $("#edit-main-btn").prop("disabled", false);
        $("#delete-btn").prop("disabled", false);
        app.editNoteView.hideView();
    });
});